Copyright (c) 2013-2015 BlueKernel, Inc.

You may use this library under the terms of the [Bluekernel Subscription Agreement][].

[Bluekernel Subscription Agreement]: http://bluekernel.ca/license
